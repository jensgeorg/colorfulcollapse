use std::cell::RefCell;
use std::collections::HashMap;
use std::ops::{Index, IndexMut};
use std::rc::Rc;
use std::sync::Arc;

use adw::gtk::{Adjustment, Button, StringList};
use adw::{prelude::*, ComboRow, PreferencesGroup, PreferencesPage, SpinRow, StatusPage};

use adw::{gtk, ActionRow, Application, ApplicationWindow, HeaderBar, PreferencesRow};
use gdk_pixbuf::{Colorspace, Pixbuf, InterpType};
use glib::clone;
use gtk::{gdk::Texture, Box, Image, ListBox, Orientation, SelectionMode};

mod color;
mod wavefield;

use color::{Component, Hsv, Rgb};
use wavefield::{Coord, WaveField};

use crate::wavefield::Direction;

#[derive(Debug)]
struct Bitmap {
    data: Vec<Rgb>,
    width: usize,
    height: usize,
}

impl Index<&Coord> for Bitmap {
    type Output = Rgb;

    fn index(&self, coords: &Coord) -> &Self::Output {
        &self.data[coords.x + coords.y * self.width]
    }
}

impl IndexMut<&Coord> for Bitmap {
    fn index_mut(&mut self, coords: &Coord) -> &mut Self::Output {
        &mut self.data[coords.x + coords.y * self.width]
    }
}

impl Bitmap {
    fn new(width: usize, height: usize) -> Self {
        Bitmap {
            data: vec![Default::default(); width * height],
            width,
            height,
        }
    }
    fn to_pixbuf(&self) -> Pixbuf {
        let mut data = vec![0; self.width * 3 * self.height];
        for y in 0..self.height {
            for x in 0..self.width {
                let src_offset = y * self.width + x;
                let dst_offset = src_offset * 3;
                data[dst_offset + 0] = self.data[src_offset].get_byte(Component::Red);
                data[dst_offset + 1] = self.data[src_offset].get_byte(Component::Green);
                data[dst_offset + 2] = self.data[src_offset].get_byte(Component::Blue);
            }
        }

        let bytes = glib::Bytes::from_owned(data);
        Pixbuf::from_bytes(
            &bytes,
            Colorspace::Rgb,
            false,
            8,
            self.width as i32,
            self.height as i32,
            3 * self.width as i32,
        ).scale_simple(64, 64, InterpType::Bilinear).unwrap()
    }

    fn to_paintable(&self) -> Texture {
        Texture::for_pixbuf(&self.to_pixbuf())
    }
}

#[derive(Debug, Default)]
struct Colors {
    c: Vec<Rgb>,
    dist: Vec<f32>,
}

impl Colors {
    pub fn update_distances(&mut self) {
        for color in &self.c {
            for c2 in self.c.clone() {
                self.dist.push(color.distance(&c2));
            }
        }
    }
}

fn main() {
    const COLORS: usize = 4;
    const TILES: usize = 5;
    const TILE_SIZE: usize = 8;
    let application = Application::builder()
        .application_id("com.example.FirstAdwaitaApp")
        .build();

    application.connect_activate(|app| {
        let page = PreferencesPage::new();
        let boxed_list = vec![String::from("boxed-list")];
        let button = Button::builder().icon_name("view-refresh-symbolic").build();
        let group = PreferencesGroup::builder()
            .title("Step 1: Colors")
            .css_classes(boxed_list.clone())
            .build();
        group.set_header_suffix(Some(&button));
        page.add(&group);

        let palette_row = ActionRow::builder().build();
        group.add(&palette_row);

        let model = StringList::new(&["Random", "Gradient"]);
        let combo = ComboRow::builder()
            .title("Color Generation Mode")
            .model(&model)
            .build();
        group.add(&combo);

        let cb = Box::new(Orientation::Horizontal, 16);
        cb.set_halign(gtk::Align::Center);

        let image1 = Image::builder().pixel_size(32).build();
        cb.append(&image1);
        let image2 = Image::builder().pixel_size(32).build();
        cb.append(&image2);
        let image3 = Image::builder().pixel_size(32).build();
        cb.append(&image3);
        let image4 = Image::builder().pixel_size(32).build();
        cb.append(&image4);
        palette_row.set_child(Some(&cb));

        let colors : Rc<RefCell<Option<Colors>>> = Rc::new(RefCell::new(None));

        let generator_clone = colors.clone();
        button.connect_clicked(clone!(@strong image1 as i1, @strong image2 as i2, @strong image3 as i3, @strong image4 as i4, @strong combo as combo => move |_| {
            let mut c = Colors::default();
            c.c =
                match combo.selected() {
                    0 => Rgb::default().take(COLORS).collect(),
                    1 => Hsv::random()
                .range(COLORS)
                .iter()
                .map(|c| c.to_rgb())
                .collect(),
                _ => panic!("Must not happen"),
            };
            c.update_distances();

            *std::cell::RefCell::<_>::borrow_mut(&generator_clone) = Some(c);

            let m = generator_clone.borrow();
            let c = m.as_ref().unwrap();
            i1.set_paintable(Some(&c.c[0].to_paintable()));
            i1.set_tooltip_text(Some(&std::format!("{:?}", c.c[0])));
            i2.set_paintable(Some(&c.c[1].to_paintable()));
            i2.set_tooltip_text(Some(&std::format!("{:?}", c.c[1])));
            i3.set_paintable(Some(&c.c[2].to_paintable()));
            i3.set_tooltip_text(Some(&std::format!("{:?}", c.c[2])));
            i4.set_paintable(Some(&c.c[3].to_paintable()));
            i4.set_tooltip_text(Some(&std::format!("{:?}", c.c[3])));

            println!("Colors: {:?}", c.c);
            println!("Distances: {:?}", c.dist);
        }));

        let group = PreferencesGroup::builder()
            .title("Step 2: Tiles")
            .css_classes(boxed_list.clone())
            .build();
        let button = Button::builder().icon_name("view-refresh-symbolic").build();
        group.set_header_suffix(Some(&button));
        page.add(&group);

        let row = ActionRow::builder().build();
        group.add(&row);

        let tiles_colors = colors.clone();
        let tiles : Rc<RefCell<Vec<Bitmap>>> = Rc::new(RefCell::new(vec![]));
        let tiles_clone = tiles.clone();
        button.connect_clicked(clone!(@strong row => move |_| {
            let cb = Box::new(Orientation::Horizontal, 16);
            cb.set_halign(gtk::Align::Center);

            let a = tiles_colors.borrow();
            let colors = a.as_ref().unwrap();

            let row2 = PreferencesRow::builder()
                .activatable(true)
                .title("Genreated tiles")
                .build();
            let cb2 = Box::new(Orientation::Horizontal, 16);
            row2.set_child(Some(&cb2));

            let mut d = colors.dist.clone();
            d.sort_by(|a, b| a.partial_cmp(b).unwrap());
            let median = d[d.len() / 2];

            let color_constraint = |a: usize, b: usize, _direction: &Direction| -> bool {
                colors.dist[b * colors.c.len() + a] < median
            };

            {
                let mut bar = std::cell::RefCell::<_>::borrow_mut(&tiles_clone);
                bar.clear();

                println!("Collapsing tile {}", 0);
                let mut wavefield = WaveField::new(TILE_SIZE, TILE_SIZE, colors.c.len());
                wavefield.collapse(color_constraint);
                let mut b = Bitmap::new(TILE_SIZE, TILE_SIZE);

                for i in 0..wavefield.field.len() {
                    if let Some(color) = wavefield.field[i].first() {
                        b.data[i] = colors.c[*color];
                    }
                }
                bar.push(b);

                println!("Collapsing tile {}", 1);
                let mut wavefield2 = WaveField::new(TILE_SIZE, TILE_SIZE, colors.c.len());
                for x in 0..TILE_SIZE {
                    let top = Coord::new(x, 0);
                    let bottom = Coord::new(x, TILE_SIZE - 1);
                    wavefield2[&bottom] = wavefield[&top].clone();

                }
                wavefield2.collapse(color_constraint);
                let mut b = Bitmap::new(TILE_SIZE, TILE_SIZE);

                for i in 0..wavefield2.field.len() {
                    if let Some(color) = wavefield2.field[i].first() {
                        b.data[i] = colors.c[*color];
                    }
                }
                bar.push(b);

                let mut wavefield3 = WaveField::new(TILE_SIZE, TILE_SIZE, colors.c.len());
                for x in 0..TILE_SIZE {
                    let top = Coord::new(x, 0);
                    let bottom = Coord::new(x, TILE_SIZE - 1);
                    wavefield3[&bottom] = wavefield2[&top].clone();
                }
                for y in 0..TILE_SIZE {
                    let left = Coord::new(0, y);
                    let right = Coord::new(TILE_SIZE - 1, y);
                    wavefield3[&left] = wavefield[&right].clone();
                    wavefield3[&right] = wavefield2[&left].clone();
                }
                wavefield3.collapse(color_constraint);
                let mut b = Bitmap::new(TILE_SIZE, TILE_SIZE);

                for i in 0..wavefield3.field.len() {
                    if let Some(color) = wavefield3.field[i].first() {
                        b.data[i] = colors.c[*color];
                    }
                }
                bar.push(b);

                let mut wavefield4 = WaveField::new(TILE_SIZE, TILE_SIZE, colors.c.len());
                for x in 0..TILE_SIZE {
                    let top = Coord::new(x, 0);
                    let bottom = Coord::new(x, TILE_SIZE - 1);
                    wavefield4[&top] = wavefield[&bottom].clone();
                    wavefield4[&bottom] = wavefield[&top].clone();
                }
                for y in 0..TILE_SIZE {
                    let left = Coord::new(0, y);
                    let right = Coord::new(TILE_SIZE - 1, y);
                    wavefield4[&left] = wavefield3[&right].clone();
                    wavefield4[&right] = wavefield2[&left].clone();
                }
                wavefield4.collapse(color_constraint);
                let mut b = Bitmap::new(TILE_SIZE, TILE_SIZE);

                for i in 0..wavefield4.field.len() {
                    if let Some(color) = wavefield4.field[i].first() {
                        b.data[i] = colors.c[*color];
                    }
                }
                bar.push(b);

                let mut wavefield5 = WaveField::new(TILE_SIZE, TILE_SIZE, colors.c.len());
                for x in 0..TILE_SIZE {
                    let top = Coord::new(x, 0);
                    let bottom = Coord::new(x, TILE_SIZE - 1);
                    wavefield5[&top] = wavefield3[&bottom].clone();
                    wavefield5[&bottom] = wavefield2[&top].clone();
                }
                for y in 0..TILE_SIZE {
                    let left = Coord::new(0, y);
                    let right = Coord::new(TILE_SIZE - 1, y);
                    wavefield5[&left] = wavefield3[&right].clone();
                    wavefield5[&right] = wavefield[&left].clone();
                }
                wavefield5.collapse(color_constraint);
                let mut b = Bitmap::new(TILE_SIZE, TILE_SIZE);

                for i in 0..wavefield5.field.len() {
                    if let Some(color) = wavefield5.field[i].first() {
                        b.data[i] = colors.c[*color];
                    }
                }
                bar.push(b);
            }

            let m = &*std::cell::RefCell::<_>::borrow(&tiles_clone);
            for a in m {
                let image = Image::new();
                image.set_paintable(Some(&a.to_paintable()));
                image.set_pixel_size(64);
                cb.append(&image);
            }

            row.set_child(Some(&cb));
        }));

        let group = PreferencesGroup::builder()
            .title("Step 3: Image")
            .css_classes(boxed_list.clone())
            .build();
        let cb = Box::new(Orientation::Horizontal, 16);
        let row = ActionRow::builder().build();
        let image_refresh = Button::builder().icon_name("view-refresh-symbolic").build();
        let image_save = Button::builder().icon_name("document-save-symbolic").build();
        let tiles_clone = tiles.clone();
        image_refresh.connect_clicked(clone!(@strong row => move |_| {
            let tile_constraint = |candidate: usize, base: usize, direction: &Direction| -> bool {
                let mut resolver = HashMap::new();
                let map = HashMap::from([
                    (Direction::LEFT, vec![4]),
                    (Direction::RIGHT, vec![2]),
                    (Direction::TOP, vec![1]),
                    (Direction::BOTTOM, vec![3])
                ]);
                resolver.insert(0, map);

                let map = HashMap::from([
                    (Direction::LEFT, vec![2, 3]),
                    (Direction::RIGHT, vec![1, 3, 4]),
                    (Direction::TOP, vec![2, 3, 4]),
                    (Direction::BOTTOM, vec![0])
                ]);
                resolver.insert(1, map);

                let map = HashMap::from([
                    (Direction::LEFT, vec![0]),
                    (Direction::RIGHT, vec![1, 3, 4]),
                    (Direction::TOP, vec![2, 3, 4]),
                    (Direction::BOTTOM, vec![1])
                ]);
                resolver.insert(2, map);

                let map = HashMap::from([
                    (Direction::LEFT, vec![1, 2, 3]),
                    (Direction::RIGHT, vec![1, 3, 4]),
                    (Direction::TOP, vec![0]),
                    (Direction::BOTTOM, vec![1, 2, 4])
                ]);
                resolver.insert(3, map);
                
                let map = HashMap::from([
                    (Direction::LEFT, vec![1, 2, 3]),
                    (Direction::RIGHT, vec![0]),
                    (Direction::TOP, vec![2, 3, 4]),
                    (Direction::BOTTOM, vec![1, 2, 4])
                ]);
                resolver.insert(4, map);

                resolver[&base][direction].contains(&candidate)
            };

            println!("Collapsing image...");
            let mut wv = WaveField::new(16, 16, 5);
            wv.collapse(tile_constraint);


            let m = &*std::cell::RefCell::<_>::borrow(&tiles_clone);
            let output = Pixbuf::new(Colorspace::Rgb, false, 8, 16 * 64, 16 * 64).unwrap();
            for x in 0..16 {
                for y in 0..16 {
                    let coord = Coord::new(x, y);
                    if let Some(tile) = wv[&coord].first() {
                        let p = &m[*tile].to_pixbuf();
                        p.copy_area(0, 0, 64, 64, &output, (x * 64) as i32, (y * 64) as i32);
                    }
                }
            }

            let paintable = Texture::for_pixbuf(&output);
            let image = Image::new();
            image.set_pixel_size(16 * 64);
            image.set_paintable(Some(&paintable));
            row.set_child(Some(&image));
        }));
        cb.append(&image_save);
        cb.append(&image_refresh);
        group.set_header_suffix(Some(&cb));
        group.add(&row);
        page.add(&group);


        let adjustment = Adjustment::new(64.0, 16.0, 800.0, 8.0, 32.0, 0.0);
        let row = SpinRow::builder()
            .title("Resolution")
            .adjustment(&adjustment)
            .build();
        group.add(&row);

        // Combine the content in a box
        let content = Box::new(Orientation::Vertical, 0);
        // Adwaitas' ApplicationWindow does not include a HeaderBar
        content.append(&HeaderBar::new());
        content.append(&page);

        let window = adw::ApplicationWindow::builder()
            .application(app)
            .title("First App")
            .default_width(350)
            // add content to window
            .content(&content)
            .build();
        window.present();
    });

    application.run();
}
