use adw::gtk::gdk::Texture;
use gdk_pixbuf::{Colorspace, Pixbuf};
use rand::Rng;

#[derive(Clone, Copy, Debug)]
pub struct Hsv {
    hue: f32,
    saturation: f32,
    value: f32,
}

#[derive(Clone, Copy, Debug)]
pub struct Rgb {
    r: f32,
    g: f32,
    b: f32,
}
pub enum Component {
    Red,
    Green,
    Blue,
}

impl Default for Rgb {
    fn default() -> Self {
        Rgb::random()
    }
}

impl Iterator for Rgb {
    type Item = Rgb;

    fn next(&mut self) -> Option<Self::Item> {
        Some(Rgb::random())
    }
}

impl Rgb {
    pub fn new(r: f32, g: f32, b: f32) -> Self {
        Rgb { r, g, b }
    }

    pub fn random() -> Self {
        let mut rng = rand::thread_rng();

        Rgb::new(
            rng.gen_range(0.0..1.0),
            rng.gen_range(0.0..1.0),
            rng.gen_range(0.0..1.0),
        )
    }

    pub fn to_paintable(&self) -> Texture {
        let p = Pixbuf::new(Colorspace::Rgb, false, 8, 128, 64).unwrap();
        p.fill(self.to_rgba());
        Texture::for_pixbuf(&p)
    }

    pub fn to_rgba(&self) -> u32 {
        let ri = self.get_byte(Component::Red);
        let gi = self.get_byte(Component::Green);
        let bi = self.get_byte(Component::Blue);

        ((ri as u32) << 24) | ((gi as u32) << 16) | (bi as u32) << 8 | 0xff
    }

    pub fn distance(&self, other: &Rgb) -> f32 {
        let r_mean = self.r + other.r / 2.0;
        let r = self.r - other.r;
        let g = self.g - other.g;
        let b = self.b - other.b;
        (((2.0 + r_mean) * r * r) + 4.0 * g * g + ((3.0 - r_mean) * b * b)).sqrt()
    }

    pub fn get_byte(&self, component: Component) -> u8 {
        match component {
            Component::Red => (self.r * 255.0).round() as u8,
            Component::Green => (self.g * 255.0).round() as u8,
            Component::Blue => (self.b * 255.0).round() as u8,
        }
    }
}

impl Hsv {
    pub fn random() -> Self {
        let mut rng = rand::thread_rng();
        Hsv {
            hue: rng.gen_range(0.0..1.0_f32),
            saturation: rng.gen_range(0.0..1.0),
            value: rng.gen_range(0.0..1.0),
        }
    }

    pub fn new(hue: f32, saturation: f32, value: f32) -> Self {
        Hsv {
            hue,
            saturation,
            value,
        }
    }

    pub fn range(&self, len: usize) -> Vec<Hsv> {
        (0..256)
            .step_by(255 / (len - 1))
            .map(|x| Hsv::new(self.hue, x as f32 / 255.0, self.value))
            .collect()
    }

    pub fn to_rgb(&self) -> Rgb {
        if self.saturation == 0.0 {
            return Rgb {
                r: self.value,
                g: self.value,
                b: self.value,
            };
        }

        let mut hue_denorm = self.hue * 360.0;
        if hue_denorm == 360.0 {
            hue_denorm = 0.0;
        }

        let hue_hexant = hue_denorm / 60.0;
        let hexant_i_part = hue_hexant as u32;
        let hexant_f_part = hue_hexant - (hexant_i_part as f32);

        let p = self.value * (1.0 - self.saturation);
        let q = self.value * (1.0 - (self.saturation * hexant_f_part));
        let t = self.value * (1.0 - (self.saturation * (1.0 - hexant_f_part)));

        match hexant_i_part {
            0 => Rgb::new(self.value, t, p),
            1 => Rgb::new(q, self.value, p),
            2 => Rgb::new(p, self.value, t),
            3 => Rgb::new(p, q, self.value),
            4 => Rgb::new(t, p, self.value),
            5 => Rgb::new(self.value, p, q),
            _ => {
                panic!("Must not happen");
            }
        }
    }
}
