use std::ops::{Index, IndexMut};

use rand::seq::{IteratorRandom, SliceRandom};

#[derive(Clone, Copy, Debug)]
pub struct Coord {
    pub x: usize,
    pub y: usize,
}

impl Coord {
    pub fn new(x: usize, y: usize) -> Self {
        Coord { x, y }
    }
}

#[derive(Debug, PartialEq, Eq, Hash)]
pub enum Direction {
    LEFT,
    RIGHT,
    TOP,
    BOTTOM,
}

pub struct CoordIterator {
    coords: Vec<(Direction, Coord)>,
}

impl CoordIterator {
    fn new(w: usize, h: usize, c: &Coord) -> Self {
        let mut it = CoordIterator { coords: Vec::new() };

        let y = c.y as i64;
        let x = c.x as i64;

        for j in (y - 1).max(0)..=(y + 1).min(h as i64 - 1) {
            let v = j as usize;
            if v == c.y {
                continue;
            }

            if v < c.y {
                it.coords.push((Direction::TOP, Coord::new(c.x, v)));
            } else {
                it.coords.push((Direction::BOTTOM, Coord::new(c.x, v)));
            }
        }

        for j in (x - 1).max(0)..=(x + 1).min(w as i64 - 1) {
            let v = j as usize;
            if v == c.x {
                continue;
            }

            if v < c.x {
                it.coords.push((Direction::LEFT, Coord::new(v, c.y)));
            } else {
                it.coords.push((Direction::RIGHT, Coord::new(v, c.y)));
            }
        }

        it
    }
}

impl Iterator for CoordIterator {
    type Item = (Direction, Coord);

    fn next(&mut self) -> Option<Self::Item> {
        self.coords.pop()
    }
}

#[derive(Debug)]
pub struct WaveField {
    pub width: usize,
    pub height: usize,
    pub field: Vec<Vec<usize>>,
}

impl WaveField {
    pub fn new(width: usize, height: usize, choices: usize) -> Self {
        WaveField {
            width,
            height,
            field: vec![(0..choices).collect::<Vec<usize>>(); width * height],
        }
    }

    pub fn random_location(&self) -> Option<Coord> {
        let mut rng = rand::thread_rng();

        let mut index_by_length: Vec<usize> = (0..(self.width * self.height))
            .filter(|&a| self.field[a].len() > 1)
            .collect();

        if index_by_length.len() == 0 {
            return None;
        }

        index_by_length.sort_by_key(|&a| self.field[a].len());
        let min = self.field[index_by_length[0]].len();
        let index = index_by_length
            .iter()
            .filter(|a| self.field[**a].len() == min)
            .choose(&mut rng)
            .unwrap();

        let y = index / self.height;
        let x = index - (y * self.height);

        Some(Coord { x: x, y: y })
    }

    pub fn adjacents(&self, c: &Coord) -> CoordIterator {
        CoordIterator::new(self.width, self.height, c)
    }

    pub fn collapse<Constraint>(&mut self, constraint: Constraint)
    where
        Constraint: Fn(usize, usize, &Direction) -> bool,
    {
        let mut rng = rand::thread_rng();
        while let Some(start_idx) = self.random_location() {
            let start_color_idx = *self[&start_idx].choose(&mut rng).unwrap();

            self[&start_idx] = vec![start_color_idx];

            for (direction, adjacent) in self.adjacents(&start_idx) {
                let val = &self[&adjacent];
                if val.len() > 1 {
                    self[&adjacent] = val
                        .iter()
                        .filter(|a| constraint(**a, start_color_idx, &direction))
                        .map(|x| usize::clone(x))
                        .collect::<Vec<usize>>();
                }
            }
        }
    }
}

impl Index<&Coord> for WaveField {
    type Output = Vec<usize>;

    fn index(&self, coords: &Coord) -> &Self::Output {
        &self.field[coords.x + coords.y * self.width]
    }
}

impl IndexMut<&Coord> for WaveField {
    fn index_mut(&mut self, coords: &Coord) -> &mut Self::Output {
        &mut self.field[coords.x + coords.y * self.width]
    }
}
